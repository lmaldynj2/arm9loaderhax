The version of autofirm used in this guide was modified from [Reboot.ms](https://www.reboot.ms/forum/threads/2403/)'s autofirm which was modified from '[Raugo](https://gbatemp.net/members/356694/)'s original autofirm, it is being rehosted [with permission](http://archive.is/KOrWp).

If you are on 11.0.0 or 11.1.0, you must follow this guide to downgrade your NATIVE_FIRM using a hardmod in order to directly dump and restore your NAND.    

**An excellent guide to getting a hardmod can be found [here](https://gbatemp.net/threads/414498/).**

This is a currently working implementation of the "FIRM partitions known-plaintext" exploit detailed [here](https://www.3dbrew.org/wiki/3DS_System_Flaws).

This will work on both a New and Old 3DS.

#### What you need

* Your NAND image extracted using your [hardmod](https://gbatemp.net/threads/414498/)
* The latest version of [autofirm](https://github.com/Plailect/autofirm/archive/master.zip)
* The firmware zip corresponding to your device and version:
    + <a href="https://plailect.github.io/Guide/11.0.0_to_10.4.0_n3ds.torrent" target="_blank">New 3DS 11.0.0 to 10.4.0</a> ([mirror](https://mega.nz/#!hgtj3QgZ!3wwyZuvUvpvM1wOr1qRAltXGXq3UZEc-sKmxQ5PRlTw)) ([mirror](https://drive.google.com/uc?export=download&id=0BzPfvjeuhqoDY1BMVExyMm9BQUE))
    + <a href="https://plailect.github.io/Guide/11.0.0_to_10.4.0_o3ds.torrent" target="_blank">Old 3DS 11.0.0 to 10.4.0</a> ([mirror](https://mega.nz/#!woMgXY7B!3n0p1pG1xdituxRuCWBlEq5J9b3ghPsMihE6T6YvttE)) ([mirror](https://drive.google.com/uc?export=download&id=0BzPfvjeuhqoDWnFHUkpHbzNSc1k))    
    ~    
    + <a href="https://plailect.github.io/Guide/11.1.0_to_10.4.0_o3ds.torrent" target="_blank">New 3DS 11.1.0 to 10.4.0</a> ([mirror](https://mega.nz/#!FxVBiD6C!ipsdaJSJPudRlA4g9r7trdyhNbnsdXg--DijbC1dYCs)) ([mirror](https://drive.google.com/uc?export=download&id=0BzPfvjeuhqoDT3B2cjJsbFBhQU0))    
    + <a href="https://plailect.github.io/Guide/11.1.0_to_10.4.0_n3ds.torrent" target="_blank">Old 3DS 11.1.0 to 10.4.0</a> ([mirror](https://mega.nz/#!8scURLDA!CRf_bqo12j4skRAUf6kAUBbw8G_80eSaMsIpCw505tw)) ([mirror](https://drive.google.com/uc?export=download&id=0BzPfvjeuhqoDaDNWLTVkb2hSb3c))    

#### Instructions

1. Extract the autofirm zip to a folder
2. Place a copy of your NAND backup (named `nand.bin`) in the autofirm folder
3. Copy the contents of the firmware zip to the autofirm folder
4. Run "autofirm.bat" and select which device and version the NAND backup is for
5. Wait while the script runs
6. If everything worked, then your original NAND will have been renamed to `backup_nand.bin` and you will have a modified `nand.bin` containing the 10.4.0 NATIVE_FIRM
7. Flash this `nand.bin` to your device with your hardmod

Your version number will *not* have changed in the settings, but the exploit has worked.

You can now continue from either [Homebrew Launcher (No Browser)](Homebrew-Launcher-(No-Browser)) or [Homebrew Launcher (Browser)](Homebrew-Launcher-(Browser)), depending on what the [Part 1 - Decrypt9](Part-1-(Decrypt9)) chart specified for your version.
